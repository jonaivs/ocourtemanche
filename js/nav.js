function setclasse(elementID, name) {
    document.getElementById(elementID).className = name;
}
function openPropos() {
    setclasse('home', 'hidden');
    setclasse('cabinet', 'hidden');
    setclasse('propos', 'visible');
}
function closeContent() {
    setclasse('propos', 'hidden');
    setclasse('cabinet', 'hidden');
    setclasse('home', 'visible');
}
function openCabinet() {
    setclasse('home', 'hidden');
    setclasse('propos', 'hidden');
    setclasse('cabinet', 'visible');
}

function rollCarte(state) {
    if (state)
        document.getElementById("lienCarte").style.textDecoration = "underline";
    else
        document.getElementById("lienCarte").style.textDecoration = "none";

}